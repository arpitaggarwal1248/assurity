package com.assurity.api.categories.Response;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Fees {
	@JsonProperty("Subtitle")
	private String Subtitle;

	@JsonProperty("Reserve")
    private String Reserve;

	@JsonProperty("Bundle")
    private String Bundle;

	@JsonProperty("Listing")
    private String Listing;

	@JsonProperty("EndDate")
    private String EndDate;

	@JsonProperty("ListingFeeTiers")
    private List<ListingFeeTiers> ListingFeeTiers;

	@JsonProperty("TenDays")
    private String TenDays;

	@JsonProperty("SecondCategory")
    private String SecondCategory;

	@JsonProperty("Gallery")
    private String Gallery;

	@JsonProperty("Feature")
    private String Feature;

    public String getSubtitle ()
    {
        return Subtitle;
    }

    public void setSubtitle (String Subtitle)
    {
        this.Subtitle = Subtitle;
    }

    public String getReserve ()
    {
        return Reserve;
    }

    public void setReserve (String Reserve)
    {
        this.Reserve = Reserve;
    }

    public String getBundle ()
    {
        return Bundle;
    }

    public void setBundle (String Bundle)
    {
        this.Bundle = Bundle;
    }

    public String getListing ()
    {
        return Listing;
    }

    public void setListing (String Listing)
    {
        this.Listing = Listing;
    }

    public String getEndDate ()
    {
        return EndDate;
    }

    public void setEndDate (String EndDate)
    {
        this.EndDate = EndDate;
    }


    public String getTenDays ()
    {
        return TenDays;
    }

    public void setTenDays (String TenDays)
    {
        this.TenDays = TenDays;
    }

    public String getSecondCategory ()
    {
        return SecondCategory;
    }

    public void setSecondCategory (String SecondCategory)
    {
        this.SecondCategory = SecondCategory;
    }

    public String getGallery ()
    {
        return Gallery;
    }

    public void setGallery (String Gallery)
    {
        this.Gallery = Gallery;
    }

    public String getFeature ()
    {
        return Feature;
    }

    public void setFeature (String Feature)
    {
        this.Feature = Feature;
    }
}
