package com.assurity.api.categories.Response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ListingFeeTiers {

	@JsonProperty("MinimumTierPrice")
	private String MinimumTierPrice;

	@JsonProperty("FixedFee")
    private String FixedFee;

    public String getMinimumTierPrice ()
    {
        return MinimumTierPrice;
    }

    public void setMinimumTierPrice (String MinimumTierPrice)
    {
        this.MinimumTierPrice = MinimumTierPrice;
    }

    public String getFixedFee ()
    {
        return FixedFee;
    }

    public void setFixedFee (String FixedFee)
    {
        this.FixedFee = FixedFee;
    }

}
