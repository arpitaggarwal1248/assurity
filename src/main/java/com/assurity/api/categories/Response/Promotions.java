package com.assurity.api.categories.Response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Promotions {

	@JsonProperty("Name")
	private String Name;

	@JsonProperty("Description")
	private String Description;

	@JsonProperty("Price")
	private String Price;

	@JsonProperty("Id")
	private String Id;

	@JsonProperty("MinimumPhotoCount")
	private String MinimumPhotoCount;

	public String getName ()
	{
		return Name;
	}

	public void setName (String Name)
	{
		this.Name = Name;
	}

	public String getDescription ()
	{
		return Description;
	}

	public void setDescription (String Description)
	{
		this.Description = Description;
	}

	public String getPrice ()
	{
		return Price;
	}

	public void setPrice (String Price)
	{
		this.Price = Price;
	}

	public String getId ()
	{
		return Id;
	}

	public void setId (String Id)
	{
		this.Id = Id;
	}

	public String getMinimumPhotoCount ()
	{
		return MinimumPhotoCount;
	}

	public void setMinimumPhotoCount (String MinimumPhotoCount)
	{
		this.MinimumPhotoCount = MinimumPhotoCount;
	}
}
