package com.assurity.api.Requests;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.LinkedHashMap;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.map.ObjectMapper;

import com.assurity.api.categories.Response.CategoriesResponse;

import util.Config;
import util.ConstructURL;
import util.JsonUtil;

public class APIRequests {

	public static CategoriesResponse categoryRequest(String categoryId)
	{
		return categoryRequest(categoryId,"false");
	}

	public static CategoriesResponse categoryRequest(String categoryId,String catalogue)
	{
		Config configInstance;
		try {
			configInstance = Config.getInstance();
			String url=configInstance.getConfig("web.Url")+MessageFormat.format(configInstance.getConfig("category.endpoint.Url"),categoryId);
			String response=	hittingGetRequest(url,"catalogue",catalogue);
			ObjectMapper mapper= new ObjectMapper();
			return mapper.readValue(response, CategoriesResponse.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unused")
	private static String hittingGetRequest(String url,LinkedHashMap<String, String> params) throws ClientProtocolException, IOException, InterruptedException {
		String request=ConstructURL.createGetRequestWithURL(url, params);
		return JsonUtil.jsonGet(request);
	}

	private static String hittingGetRequest(String url,String key,String value) throws ClientProtocolException, IOException, InterruptedException {
		String request=ConstructURL.createGetRequestWithURL(url,key,value);
		return JsonUtil.jsonGet(request);
	}
}
