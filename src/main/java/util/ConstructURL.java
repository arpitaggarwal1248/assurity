package util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.http.client.utils.URIBuilder;

public class ConstructURL {

	public static String createGetRequestWithURL(String url,LinkedHashMap<String, String> params)
	{
		URIBuilder builder = new URIBuilder();
		builder.setPath(url);
		addParams(params, builder);
		return builder.toString();
	}
	
	public static String createGetRequestWithURL(String url,String key,String value)
	{
		URIBuilder builder = new URIBuilder();
		builder.setPath(url);
		builder.setParameter(key,value);
		return builder.toString();
	}


	private static void addParams(LinkedHashMap params, URIBuilder builder) {
		if(params!=null)
		{
			Set entrySet = params.entrySet();
			Iterator it= entrySet.iterator();
			while(it.hasNext()){
				Map.Entry param = (Map.Entry)it.next();
				if(param.getKey()!=null && !param.getKey().toString().isEmpty())
					builder.setParameter(param.getKey().toString(),param.getValue()!=null?param.getValue().toString():"");
			}
		}
	}


	public static String createGetRequest(String scheme,String host,String path,LinkedHashMap<String, String> params)
	{
		URIBuilder builder= new URIBuilder()
				.setScheme(scheme)
				.setHost(host)
				.setPath(path);
		addParams(params, builder);
		return builder.toString();
	}
}
