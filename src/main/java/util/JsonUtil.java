package util;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

public class JsonUtil {


	public static String jsonPostWithoutAuth(String apiurl, String apiBody ){

		RequestSpecBuilder builder = new RequestSpecBuilder();

		builder.setBody(apiBody);

		builder.setContentType("application/json; charset=UTF-8");

		RequestSpecification requestSpec = builder.build();

		Response response = given().spec(requestSpec).post(apiurl);

		String res = response.body().asString();
		return res;
	}

	public static String jsonGet(String url)
	{
		Response response=given().get(url);
		String res = response.body().asString();
		return res;
	}

	public static String jsonGetWithPassword(String url,String userName,String password)
	{
		Response response=given().authentication().preemptive().basic(userName, password).get(url);
		String res = response.body().asString();
		return res;
	}
	
	public static String jsonDelete(String url)
	{
		int response=given().delete(url).getStatusCode();
		return Integer.toString(response);
	}

	public static String jsonDeleteResponse(String url,String userName,String password)
	{
		Response resp= given().authentication().preemptive().basic(userName, password).delete(url);
		String response=resp.body().asString();
		return response;
	}
	
	public static String jsonPostUpdateWithUserPassword(String apiUrl,String apiBody,String userName,String password)
	{
		String res=null;
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setBody(apiBody);
		builder.setContentType("application/json; charset=UTF-8");
		RequestSpecification requestSpec = builder.build();
		Response response = given().authentication().preemptive().basic(userName, password).spec(requestSpec).when().put(apiUrl);
		res = response.body().asString();
		return res;
	}

	public static String jsonPostWithUserNamePassword(String APIUrl, String APIBody,String userName,String password ){
		String res=null;
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setBody(APIBody);
		builder.setContentType("application/json; charset=UTF-8");
		RequestSpecification requestSpec = builder.build();
		Response response = given().authentication().preemptive().basic(userName, password)
				.spec(requestSpec).when().post(APIUrl);
		res = response.body().asString();
		return res;
	}

}
