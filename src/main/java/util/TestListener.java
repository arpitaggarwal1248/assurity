package util;

import java.io.File;
import java.util.Arrays;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TestListener implements ITestListener {

	public static ExtentReports report;
	public ExtentTest test;



	@Override
	public void onFinish(ITestContext res) {
		report.flush();
		report.close();
	}

	@Override
	public void onStart(ITestContext arg0) {
		try {
			report=new ExtentReports(System.getProperty("user.dir")+Config.getInstance().getConfig("extent.html"),true);
			report.loadConfig(new File(System.getProperty("user.dir")+Config.getInstance().getConfig("extent.xml")));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailure(ITestResult res) {
		addLogs();
		test.log(LogStatus.FAIL, res.getName());
		test.log(LogStatus.INFO, res.getThrowable());
		
	}

	private void addLogs()
	{
		for(String log:Reporter.getOutput())
			test.log(LogStatus.INFO,log);
		Reporter.clear();
		report.endTest(test);
	}

	@Override
	public void onTestSkipped(ITestResult res) {
		addLogs();
		test.log(LogStatus.SKIP, res.getName());
		test.log(LogStatus.INFO, res.getThrowable());
		
	}

	@Override
	public void onTestStart(ITestResult res) {
		test = report.startTest("Test name : " + res.getName());
		test.log(LogStatus.INFO, "TestData: "+Arrays.asList(res.getParameters()));
		test.log(LogStatus.INFO, "Description: "+res.getMethod().getDescription());
	}

	@Override
	public void onTestSuccess(ITestResult res) {
		addLogs();
		test.log(LogStatus.PASS, res.getName());
		
	}
}
