package com.assurity.testScripts;

import java.util.List;
import java.util.stream.Collectors;

import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.assurity.api.Requests.APIRequests;
import com.assurity.api.categories.Response.CategoriesResponse;
import com.assurity.api.categories.Response.Promotions;

import util.TestListener;

@Listeners(TestListener.class)
public class CategoryTestCases {


	@Test (description="To validate Acceptance criteria for category API",dataProvider="categoryAcceptanceTestData")
	public void categoryAcceptanceCriteria(String categoryName,String categoryId,String promoName) throws Exception
	{
		Reporter.log("CategoryId: "+categoryId);
		if(categoryId==null)
			throw new SkipException("Category id is null");
		SoftAssert softAssert= new SoftAssert();
		Reporter.log("Hitting category API");
		CategoriesResponse response=APIRequests.categoryRequest(categoryId);
		Reporter.log("validating results");
		softAssert.assertEquals(response.getName(), categoryName);
		softAssert.assertEquals(response.isCanRelist(), true,"Error with can relist flag");
		List<Promotions> promoList=response.getPromotions().stream().filter(x->x.getName().equals(promoName)).collect(Collectors.toList());
		if(promoList.size()==0)
			softAssert.fail("No promo found with name Gallery");
		else
			softAssert.assertTrue(promoList.get(0).getDescription().contains("2x larger image"),"Promo Description is wrong, Actual: "+promoList.get(0).getDescription()+" Expected: 2x larger image");
		softAssert.assertAll();
	}	

	@DataProvider(name="categoryAcceptanceTestData")
	public Object[][] categoryTestData()
	{
		return new Object[][]{{"Carbon credits","6327","Gallery"},
			{"Badges","6328","Gallery"},
			{"Badges",null,"Gallery"},
			{"Carbon credits","6328","Gallery"}};
	}
}
