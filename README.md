# assurity

#INTRODUCTION
A sample test suite to test the acceptance criteria of an API

#GETTING STARTED
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

##Prerequisites:
*	Java 8
*	Maven
*	TestNG

##Installing:

###Install java on Ubuntu:

####Commands:

```
*	sudo apt-get update
*	sudo apt-get install default-jdk
```

###Install java on Windows:

Go to page and download java executor.
https://java.com/en/download/manual.jsp

Run executor and install java.


#RUNNING THE TESTS

Go to path 

```
/TestXML/CategoryTest.xml
```

Right click and run as TestNG

#### OR

Go to path

```
/src/test/java/com/assurity/testScripts/CategoryTestCases.java
```

RightClick and select run as TestNG


#REPORTING
For reporting extent reports is used. Reports will be created at below path

```
Path: /ExtentReports
```

Sample report is present at same path with name: SampleExtentReport.html





